# Netbox Docker

Self-hosted NetBox instance with a TLS reverse proxy.

## Requirements

- a Linux host somewhere that can expose TCP ports 80 and 443 to the internet.

- Docker

- A domain name that you can point to the IP address of your NetBox host.
